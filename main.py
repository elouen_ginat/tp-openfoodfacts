from typing import Optional

from fastapi import FastAPI

import requests

app = FastAPI()


@app.get("/")
def read_root():
    
    requete = requests.get("https://world.openfoodfacts.org/api/v0/product/3256540001305.json")
    print(requete.status_code)
    print(requete.json)

    return {"isVegan": isVegan(requete.json())}

@app.get("/isVegan/{item_id}")
def read_root(item_id: int, q: Optional[str] = None):
    
    requete = requests.get("https://world.openfoodfacts.org/api/v0/product/"+str(item_id)+".json")
    print(requete.status_code)
    print(requete.json)

    return {"isVegan": isVegan(requete.json())}

@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}


def isVegan(requestAsJson):
    ingredients = requestAsJson["product"]["ingredients"]
    for ingredient in ingredients:
        if 'vegan' in ingredient:
          if ingredient["vegan"]=='no' or ingredient["vegan"]=='maybe':
            return False
    return True

