# OpenFoodFacts

## installer les dépendances:

`pip install -r requirements.txt`

## Lancer l'application:

`uvicorn main:app --reload`

## accéder à l'application:

[Application](http://127.0.0.1:8000/)

## Méthode ajoutée:

`http://127.0.0.1:8000/isVegan/{id_produit} -> est vegan ou non`

